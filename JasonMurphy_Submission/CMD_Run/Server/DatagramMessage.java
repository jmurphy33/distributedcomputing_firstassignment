
import java.net.*;

public class DatagramMessage{
   private String message;
   private InetAddress senderAddress;
   private int senderPort;
   private byte[] fileBytes;

   public void setValues(String message, InetAddress addr, int port) {
      this.message = message;
      this.senderAddress = addr;
      this.senderPort = port;
   }


   public String getMessage( ) {
      String ans = this.message.replace("\u0000", "");
      String result = ans;
      return result;
   }

   public InetAddress getAddress( ) {
      return this.senderAddress;
   }

   public int getPort( ) {
      return this.senderPort;
   }
} // end class  
