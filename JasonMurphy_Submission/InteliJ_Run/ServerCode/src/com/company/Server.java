package com.company;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;

public class Server {

    static MyServerDatagramSocket mySocket;
    static String rootPath = "";
    static LinkedList<String> connected_users;
    static int port = 13;
    static String name, password, fileName, bytesToSave;

    public static void main(String[] args) {

        InputStreamReader is = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(is);
        connected_users = new LinkedList<String>();

        try {
            System.out.println("Path to store files (leave blank to store @ C:/JasonsServer): ");
            rootPath = checkVariable(br.readLine(), "C:/JasonsServer");
            createRoot(rootPath);
            System.out.println("Port number (leave blank to set to 13): ");
            port = Integer.parseInt(checkVariable(br.readLine(), "13"));
            mySocket = new MyServerDatagramSocket(port);

            while (true) {
                DatagramMessage request = mySocket.receiveMessageAndSender();
                String code = parseCode(request.getMessage());
                try {
                    // login
                    if (code.equals("100")) {
                       handleLogin(request);
                    }

                    // disconnect
                    if (code.equals("200")) {
                        handleLogout(request);
                    }

                    // upload
                    if (code.equals("400")) {
                        handleUpload(request);
                    }

                    // view files in folder
                    if (code.equals("500")) {
                        handleViewFiles(request);
                    }

                    // download
                    if (code.equals("300")) {
                        handleDownload(request);
                    }

                }
                // catch any exceptions but keep going through while loop
                catch (Exception e) {
                    System.out.println("Error: " + e.getMessage());
                    mySocket.sendMessage(request.getAddress(), request.getPort(), "400");
                }
            }

        }
        // catch any exceptions but server cannot start
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }


    static private void handleLogin(DatagramMessage request) throws Exception {
        // checks message length is correct size.
        if (isTooMany(request.getMessage(), 3,false, "/")) {
            System.out.println("Message length incorrect");
            mySocket.sendMessage(request.getAddress(), request.getPort(), "401"); // failure
        } else {
            name = parseMessage(request.getMessage(),3,1,"/");
            System.out.println(name + " connecting");
            password = parseMessage(request.getMessage(),3,2,"/");
            String created = checkDirectory(rootPath + "/" + name);
            mySocket.sendMessage(request.getAddress(), request.getPort(), created); // success

            if (created.equals("200")) connected_users.add(name);
            System.out.println(name +" is now a connected user");
        }
    }

    static private void handleLogout(DatagramMessage request) throws Exception {
        // checks message length is correct size.
        if (isTooMany(request.getMessage(), 2,false,"/")) {
            System.out.println("Message length incorrect");
            mySocket.sendMessage(request.getAddress(), request.getPort(), "401"); // failure
        } else {
            name = parseMessage(request.getMessage(),2,1,"/");
            // checks if user is connected
            if (connected_users.contains(name)) {
                System.out.println(name + " has disconnected");
                connected_users.remove(name);
                mySocket.sendMessage(request.getAddress(), request.getPort(), "200"); // success
            } else {
                System.out.println(name +" is not a connected user");
                mySocket.sendMessage(request.getAddress(), request.getPort(), "402"); // failure
            }
        }
    }

    static private void handleUpload(DatagramMessage request) throws Exception {
        // checks message length is correct size.
        if (isTooMany(request.getMessage(), 3,true, "/")) {
            System.out.println("Message length incorrect");
            mySocket.sendMessage(request.getAddress(), request.getPort(), "401");
        } else {
            name = parseMessage(request.getMessage(),4,1,"/");
            // checks if user is connected
            if (connected_users.contains(name)) {
                System.out.println(name+ " wants to upload");
                fileName = parseMessage(request.getMessage(),4,2,"/");
                bytesToSave = parseMessage(request.getMessage(),4,3,"/");
                byte[] byteArray = bytesToSave.getBytes();
                saveFile(byteArray, name, fileName);
                mySocket.sendMessage(request.getAddress(), request.getPort(), "200");
                System.out.println("Uploaded successful");
            } else {
                System.out.println(name +" is not a connected user");
                mySocket.sendMessage(request.getAddress(), request.getPort(), "402");
            }
        }


    }

    static private void handleDownload(DatagramMessage request) throws Exception {
        // checks message length is correct size.
        if (isTooMany(request.getMessage(), 3,true, "/")) {
            System.out.println("Message length incorrect");
            mySocket.sendMessage(request.getAddress(), request.getPort(), "401"); // failure
        } else {
            name = parseMessage(request.getMessage(),3,1,"/");
            // checks if user is connected
            if (connected_users.contains(name)) {
                fileName = parseMessage(request.getMessage(), 3, 2,"/");
                System.out.println(name+ " wants to download");
                File f = new File(rootPath + "/" + name + "/" + fileName);
                // checks if file exits
                if (!f.exists()) {
                    System.out.println(f.getAbsoluteFile()+" does not exist");
                    mySocket.sendMessage(request.getAddress(), request.getPort(), "403"); // failure
                } else {
                    byte[] b = Files.readAllBytes(Paths.get(rootPath + "/" + name + "/" + fileName));
                    String decoder2 = new String(b, "UTF-8");
                    decoder2 = removeNulls(decoder2);
                    mySocket.sendMessage(request.getAddress(), request.getPort(), "200/" + decoder2); // success
                    System.out.println("File sent to "+name+ " successfully");
                }
            } else {
                System.out.println(name +" is not a connected user");
                mySocket.sendMessage(request.getAddress(), request.getPort(), "402"); // failure
            }
        }
    }

    static private String removeNulls(String msg){
        String ans = msg.replace("\u0000", "");
        return  ans;
    }
    static private void handleViewFiles(DatagramMessage request) throws Exception {
        // checks if user is connected
        if (isTooMany(request.getMessage(), 2, false, "/")) {
            System.out.println("Message length incorrect");
            mySocket.sendMessage(request.getAddress(), request.getPort(), "401"); // failure
        } else {
            name = parseMessage(request.getMessage(),2,1,"/");

            System.out.println(name+ " wants to view files");
            // checks if user is connected
            if (connected_users.contains(name)) {
                String fileList = getFileList(name);
                mySocket.sendMessage(request.getAddress(), request.getPort(), "200/" + fileList); // success
            } else {
                System.out.println(name +" is not a connected user");
                mySocket.sendMessage(request.getAddress(), request.getPort(), "402"); // failure
            }
        }
    }

    // if the user inputs an empty string this will correct to a default value
    static private String checkVariable(String input, String output) {
        // if the user inputs an empty string this will correct to a default value

        if (input.equals("")) {
            return output;
        } else return input;
    }
    // this will create root directory for all user folders to go under
    static private void createRoot(String root) {
        boolean isCreated = new File(root).mkdirs();

        // if something was created print it out.
        if (isCreated) {
            System.out.println("root director created at:" + root);
        }
    }

    // this will check if the length of the message is too long/short
    static private Boolean isTooMany(String message, int size, Boolean applyGreaterThan, String splitBy) {

        // splits by the character /
        String[] m = message.split(splitBy);

        // applied for download/uploads when messaqge lengths will vary
        if(applyGreaterThan){
            if(size <= m.length) return  false;
            else return  true;
        }
        else{
            if (size == m.length) return false;
            else return true;
        }
    }

    // used to parse the first three characters of the message
    static private String parseCode(String message) {
        String str = message.substring(0, 3);
        return str;
    }



    // used to break the message down per element
    static private String parseMessage(String message, int num, int take, String splitBy) {
        // messages are broken down by / character. The num value is the number of splits needed, the take is the
        // element wanted after the split.

        String[] m = message.split(splitBy,num);
        return m[take];
    }

    // writes file to the users folder using the file bytes and name of file
    static private void saveFile(byte[] fileBytes, String name, String fileName) {
        try {

            FileOutputStream fos = new FileOutputStream(rootPath + "\\" + name + "\\" + fileName);
            fos.write(fileBytes);
            fos.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    // will create a directory for the user
    static private String checkDirectory(String name) {
        try {
            File f = new File(name);
            // if already exists return 200 everything was successful
            if (f.exists()) {
                return "200";
            } else {
                boolean isCreated = new File(name).mkdirs(); // if doesn't exist, create it.

                // simple print to know if something ws created or a problem
                if (isCreated) {
                    System.out.println("\ncreated folder for user: " + name);
                    return "200";
                } else {
                    System.out.println("\nproblem with: " + name);
                    return "400";
                }
            }
        } catch (SecurityException e) {
            System.out.println(e.getMessage());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return "400";
    }

    // gets a list of files for user. returns file names
    static private String getFileList(String name) {
        String allFiles = "";
        try {
            String path = rootPath + "\\" + name;
            File folder = new File(path);
            File[] listOfFiles = folder.listFiles();
            // build string with all files in directory
            for (int i = 0; i < listOfFiles.length; i++) {
                allFiles = allFiles + "\n" + listOfFiles[i].getName();
            }
            // if no files build other string
            if (listOfFiles.length == 0) allFiles = "No files found...try uploading some first";

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return allFiles;
    }

}
