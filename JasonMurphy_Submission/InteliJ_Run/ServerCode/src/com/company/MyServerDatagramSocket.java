
package com.company;
import java.net.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;



public class MyServerDatagramSocket extends DatagramSocket {
static final int MAX_LEN = 10000;

   MyServerDatagramSocket(int portNo) throws SocketException{
     super(portNo);
   }

   public void sendMessage(InetAddress receiverHost,int receiverPort,String message) throws IOException
   {
         byte[ ] sendBuffer = message.getBytes( );                                     
         DatagramPacket datagram =  new DatagramPacket(sendBuffer, sendBuffer.length,receiverHost, receiverPort);
         this.send(datagram);
   }


    public String receiveMessage( ) throws IOException {
        byte[ ] receiveBuffer = new byte[MAX_LEN];
        DatagramPacket datagram = new DatagramPacket(receiveBuffer, MAX_LEN);
        this.receive(datagram);
        String message = new String(receiveBuffer);
        return message;
    } //end receiveMessage


   public DatagramMessage receiveMessageAndSender( )throws IOException
   {

         byte[ ] receiveBuffer = new byte[MAX_LEN];
         DatagramPacket datagram = new DatagramPacket(receiveBuffer, MAX_LEN);
         this.receive(datagram);
         DatagramMessage returnVal = new DatagramMessage( );
         returnVal.setValues(new String(receiveBuffer), datagram.getAddress(), datagram.getPort());
         return returnVal;
   } //end receiveMessage
} //end class
