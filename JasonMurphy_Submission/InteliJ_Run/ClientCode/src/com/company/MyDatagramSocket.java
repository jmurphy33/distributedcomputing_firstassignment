package  com.company;
import java.net.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class MyDatagramSocket extends DatagramSocket {
   static final int MAX_LEN = 100000;
   MyDatagramSocket( )  throws SocketException{
     super( );
   }
   MyDatagramSocket(int portNo)  throws SocketException{
     super(portNo);
   }
   public void sendMessage(InetAddress receiverHost, int receiverPort, String msg) throws IOException {
         byte[ ] sendBuffer = msg.getBytes( );
         DatagramPacket datagram =  new DatagramPacket(sendBuffer, sendBuffer.length,receiverHost, receiverPort);
         this.send(datagram);
   } // end sendMessage



   public String receiveMessage()throws IOException {
         byte[ ] receiveBuffer = new byte[MAX_LEN];
         DatagramPacket datagram = new DatagramPacket(receiveBuffer, MAX_LEN);
         this.receive(datagram);
         String message = new String(receiveBuffer);
         return message;
   } //end receiveMessage
} //end class
