
import java.io.*;
import java.net.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ClientHelper {
    String code;
    MyDatagramSocket mySocket;
    InetAddress serverHost;
    int serverPort;
    String returningMessage = "";


    // parses the file bytes from message
    private String parseBytes(String message) {
        String[] m = message.split("/", 2);
        return m[1];
    }

    // removes the nulls that are present from max_len
    private String removeNulls(String msg){
        String ans = msg.replace("\u0000", "");
        return  ans;
    }
    // parses codes to more readable values
    private String parseCode(String message) {
        String str = message.substring(0, 3);

        if(str.equals("200")){ return  "(200) Success";}

        if(str.equals("400")){ return  "(400) Server Error";}

        if(str.equals("401")){ return  "(401) Message Length Incorrect";}

        if(str.equals("402")){return  "(402) User not connected";}

        if(str.equals("403")){return  "(403) File not found";}

        return str;
    }

    // builds login message
    public String loginMessage(String hostName, String portNum, String username, String password) {
        try {
            code = "100";
            setServerDetails(hostName, portNum);
            mySocket = new MyDatagramSocket();
            // build message
            String loginDetails = code + "/" + username + "/" + password;
            mySocket.sendMessage(serverHost, serverPort, loginDetails);
            // parse code for output
            returningMessage = parseCode(mySocket.receiveMessage());

        } catch (Exception e) {
            return e.getMessage();
        }

        return returningMessage;
    }

    // builds logout message
    public String logoutMessage(String hostName, String portNum, String username) {
        try {
            code = "200";
            setServerDetails(hostName, portNum);
            mySocket = new MyDatagramSocket();

            // logout + build message
            mySocket.sendMessage(serverHost, serverPort, code + "/" + username);

            // return result
            returningMessage = parseCode(mySocket.receiveMessage());
        } catch (Exception e) {

            return e.getMessage();
        }
        return returningMessage;
    }

    // builds view files message
    public String viewFilesMessage(String hostName, String portNum, String username) {
        try {


            code = "500";
            setServerDetails(hostName, portNum);
            mySocket = new MyDatagramSocket();

            // view files and creating message
            mySocket.sendMessage(serverHost, serverPort, code + "/" + username);

            String message = mySocket.receiveMessage();

            // return result
            returningMessage =  parseCode(message) +"\n"+removeNulls(message);
        } catch (Exception e) {

            return e.getMessage();
        }

        return returningMessage;
    }

    // builds upload message
    public String uploadMessage(String hostName, String portNum, String user, File file) {
        try {

            code = "400";
            setServerDetails(hostName, portNum);
            mySocket = new MyDatagramSocket();

            // convert to bytes

            byte[] b = Files.readAllBytes(Paths.get(file.getPath().toString()));
            String decoder = new String(b, "UTF-8");
            decoder = removeNulls(decoder);
            // create message
            String uploadDetails = code + "/" + user + "/" + file.getName() + "/" +decoder ;
            
            // upload
            mySocket.sendMessage(serverHost, serverPort, uploadDetails);

            // return result
            returningMessage = parseCode(mySocket.receiveMessage());

        } catch (Exception e) {
            return e.getMessage();
        }

        return returningMessage;
    }

    // build download message
    public String downloadMessage(String hostName, String portNum, String user, String fileName, String downloadPath) {
        try {
            // sending
            code = "300";
            setServerDetails(hostName, portNum);
            mySocket = new MyDatagramSocket();
            // build message
            String downloadDetails = code + "/" + user + "/" + fileName;

            // download
            mySocket.sendMessage(serverHost, serverPort, downloadDetails);

            // receiving
            String message = mySocket.receiveMessage();

            // get status code
            String code = parseCode(message);

            // not success return error message
            if (!code.equals("(200) Success")) {
                returningMessage = code;
            } else {

                // get file bytes
                String bytesToSave = parseBytes(message);
                byte[] byteArray = bytesToSave.getBytes();

                // set output message
                returningMessage = code;

                // print saved directory
                String path = downloadPath + "\\" + fileName;
                System.out.println("Saved to: " + path);

                // write file
                FileOutputStream fos = new FileOutputStream(path);
                fos.write(byteArray);
                fos.close();
            }
        } catch (Exception e) {
            return "Error: " + e.getMessage();
        }
        return returningMessage;
    }

    private void setServerDetails(String hostName, String portNum) {
        try {
            serverHost = InetAddress.getByName(hostName);
            serverPort = Integer.parseInt(portNum);
        } catch (Exception e) {

        }
    }


} //end class
