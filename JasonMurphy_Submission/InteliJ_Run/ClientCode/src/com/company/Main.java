package com.company;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.*;

public class Main {
    public static void main(String[] args) {
        InputStreamReader is = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(is);
        String hostName = "", portNum = "";
        try {
            ClientHelper ch = new ClientHelper();

            System.out.println("Host name (leave blank for localhost): ");
            hostName = checkVariable(br.readLine(), "localhost");


            System.out.println("Port number (leave blank for port 13): ");
            portNum = checkVariable(br.readLine(), "13");


            System.out.println("* Username: ");
            String user = checkUsername(br.readLine(), br);

            System.out.println("* Password: ");
            String password = checkVariable(br.readLine(), "a");

            // login
            String msg = ch.loginMessage(hostName, portNum, user, password);


            System.out.println(msg);


            if (msg.equals("400")) {
                System.out.println("Login failed");
                System.exit(0);
            } else {
                System.out.println("-----------------------------------------------\n" +
                        "* Would you like to:\nUpload (1)\nDownload (2)\nView files (3)\nLogout(4)");
                String option = br.readLine();

                while (!option.equals("4")) {

                    if (option.equals("1")) {
                        File f = getFile();
                        System.out.println(ch.uploadMessage(hostName, portNum, user, f));
                    }

                    if (option.equals("2")) {
                        System.out.println("Include the extension. Example test.txt ");
                        System.out.println("Exclude any file paths. Example do not use Hello\\test.txt ");
                        System.out.println("TIP: Use view files first to see file names. ");
                        System.out.println("* File name to download : ");
                        String filename = br.readLine();
                        String path = getFolder();
                        System.out.println(ch.downloadMessage(hostName, portNum, user, filename, path));
                    }

                    if (option.equals("3")) {
                        msg = ch.viewFilesMessage(hostName, portNum, user);
                        System.out.println(msg);
                    }

                    System.out.println("-----------------------------------------------\n" +
                            "* Would you like to:\nUpload (1)\nDownload (2)\nView files (3)\nLogout(4)");
                    option = br.readLine();
                }

                if (option.equals("4")) {
                    msg = ch.logoutMessage(hostName, portNum, user);
                    System.out.println(msg);
                    System.exit(0);
                }
            }


        } // end try
        catch (Exception ex) {
            ex.printStackTrace();

        } // end catch

    } //end main

    // gets file for upload
    static File getFile() {
        System.out.println("JFileChoose should now appear. If it doesn't press alt+tab to switch to it...");
        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("TEXT FILES", "txt", "text");
        chooser.setFileFilter(filter);
        int app = chooser.showOpenDialog(null);

        if (app == JFileChooser.APPROVE_OPTION) {
            File f = chooser.getSelectedFile();
            return f;
        }
        return null;
    }

    // gets folder for download
    static String getFolder() {
        System.out.println("JFileChoose should now appear. If it doesn't press alt+tab to switch to it...");
        JFileChooser chooser = new JFileChooser();
        chooser.setCurrentDirectory(new java.io.File("."));
        chooser.setDialogTitle("chooser");
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.setAcceptAllFileFilterUsed(false);

        if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
            System.out.println(chooser.getSelectedFile().getAbsolutePath());
            return chooser.getSelectedFile().getAbsolutePath();
        } else return "";

    }

    // sets default variable if blank is entered
    static String checkVariable(String input, String output) {
        if (input.equals("")) {
            return output;
        } else return input;
    }

    static String checkIllegalCahr(String user, BufferedReader br) throws Exception {
        while (user.equals("#")) {
            System.out.println("Files on server cannot contain # character: ");
            user = br.readLine();
        }
        return user;
    }

    // user enters blank name, re-enter name
    static String checkUsername(String user, BufferedReader br) throws Exception {
        while (user.equals("")) {
            System.out.println("* Username cannot be left blank re-enter username: ");
            user = br.readLine();
        }
        return user;
    }

} // end class